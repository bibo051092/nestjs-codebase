export const envConfig = () => ({
  // port
  PORT: parseInt(process.env.PORT, 10) || 3000,

  // database
  NODE_ENV: process.env.NODE_ENV,
  DB_HOST: process.env.DB_HOST,
  DB_PORT: parseInt(process.env.DB_PORT, 10) || 5432,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_DATABASE: process.env.DB_DATABASE,

  // redis
  REDIS_HOST: process.env.REDIS_HOST,
  REDIS_PORT: parseInt(process.env.REDIS_PORT, 10) || 6379,
  REDIS_TTL: parseInt(process.env.REDIS_TTL, 10) || 3600000,
  REDIS_KEY_PREFIX: process.env.REDIS_KEY_PREFIX,

  // rate limit
  THROTTLE_TTL: parseInt(process.env.THROTTLE_TTL, 10) || 60000,
  THROTTLE_LIMIT: parseInt(process.env.THROTTLE_LIMIT, 10) || 10,

  // jwt access token
  JWT_ACCESS_TOKEN_SECRET: process.env.JWT_ACCESS_TOKEN_SECRET,
  JWT_ACCESS_TOKEN_EXPIRATION: parseInt(process.env.JWT_ACCESS_TOKEN_EXPIRATION, 10),

  // jwt refresh token
  JWT_REFRESH_TOKEN_SECRET: process.env.JWT_REFRESH_TOKEN_SECRET,
  JWT_REFRESH_TOKEN_EXPIRATION: parseInt(process.env.JWT_REFRESH_TOKEN_EXPIRATION, 10),

  // email
  MAIL_HOST: process.env.MAIL_HOST,
  MAIL_PORT: parseInt(process.env.MAIL_PORT, 10) || 1025,
  MAIL_IGNORE_TLS: process.env.MAIL_IGNORE_TLS,
  MAIL_SECURE: process.env.MAIL_SECURE,
  MAIL_FROM: process.env.MAIL_FROM,
  MAIL_USER: process.env.MAIL_USER,
  MAIL_PASS: process.env.MAIL_PASS,
});
