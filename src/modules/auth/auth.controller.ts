import { Body, Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from '@modules/auth/local/local-auth.guard';
import { Public, ResponseMessage, User } from '@shared/decorators/common.decorator';
import { IUser } from '@modules/users/users.interface';
import { CreateUserDto } from '@modules/users/dto/create-user.dto';
import { Request, Response } from 'express';
import { ForgotPasswordDto, ResetPasswordDto } from '@modules/auth/dto/forgot-password.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @ResponseMessage('Login a user')
  @Post('login')
  login(@Req() req: any, @Res({ passthrough: true }) res: Response) {
    return this.authService.login(req.user, res);
  }

  @Public()
  @ResponseMessage('Register a new user')
  @Post('register')
  register(@Body() createUserDto: CreateUserDto) {
    return this.authService.register(createUserDto);
  }

  @ResponseMessage('Get user information')
  @Get('profile')
  profile(@User() user: IUser) {
    return user;
  }

  @ResponseMessage('Logout a User')
  @Post('logout')
  logout(@Req() req: Request, @Res({ passthrough: true }) response: Response, @User() user: IUser) {
    return this.authService.logout(req.cookies['refresh_token'], response, user);
  }

  @Public()
  @ResponseMessage('Get user by refresh token')
  @Get('refresh')
  refreshToken(@Req() req: Request, @Res({ passthrough: true }) response: Response) {
    return this.authService.refreshToken(req.cookies['refresh_token'], response);
  }

  @Public()
  @ResponseMessage('Forgot password user by email')
  @Post('forgot-password')
  async forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto) {
    return this.authService.handleForgotPassword(forgotPasswordDto.email);
  }

  @Public()
  @ResponseMessage('Reset password user by email')
  @Post('reset-password')
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return await this.authService.handleResetPassword(
      resetPasswordDto.token,
      resetPasswordDto.new_password,
    );
  }
}
