import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from '@modules/users/users.service';
import { checkPassword, getCurrentDate } from '@shared/utils/common.util';
import { IUser } from '@modules/users/users.interface';
import { JwtService } from '@nestjs/jwt';
import ms from 'ms';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from '@modules/users/dto/create-user.dto';
import { Response } from 'express';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
    private mailerService: MailerService,
  ) {}

  validateUser = async (email: string, pass: string): Promise<any> => {
    const user = await this.usersService.findByEmail(email);
    if (user && checkPassword(pass, user.password)) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result;
    }
    return null;
  };

  login = async (user: IUser, response: Response) => {
    return this.handleToken(user, response);
  };

  register = async (createUserDto: CreateUserDto) => {
    return await this.usersService.create(createUserDto);
  };

  handleToken = async (user: IUser, response: Response) => {
    const { user_id, email, first_name, last_name } = user;
    const payload: IUser = { user_id, first_name, last_name, email };
    const refresh_token: string = this.createRefreshToken(payload);
    await this.usersService.updateRefreshToken(refresh_token, user_id);

    const expires_in = ms(this.configService.get<string>('JWT_ACCESS_TOKEN_EXPIRATION')) / 1000;

    response.clearCookie('refresh_token');
    response.cookie('refresh_token', refresh_token, {
      maxAge: ms(this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRATION')),
      httpOnly: true,
    });

    return {
      access_token: this.jwtService.sign(payload),
      expires_in,
      user: {
        user_id,
        first_name,
        last_name,
        email,
      },
    };
  };

  createRefreshToken(payload: object) {
    return this.jwtService.sign(payload, {
      secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      expiresIn: ms(this.configService.get<string>('JWT_REFRESH_TOKEN_EXPIRATION')) / 1000,
    });
  }

  logout = async (refreshToken: string, response: Response, user: IUser) => {
    try {
      this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      });

      await this.usersService.updateRefreshToken('', user.user_id);
      response.clearCookie('refresh_token');
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  };

  refreshToken = async (refreshToken: string, response: Response) => {
    try {
      this.jwtService.verify(refreshToken, {
        secret: this.configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      });

      const user = await this.usersService.findUserByRefreshToken(refreshToken);

      return await this.handleToken(user, response);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  };

  handleForgotPassword = async (email: string) => {
    const { user_id } = await this.usersService.findByEmail(email);
    if (!user_id) {
      throw new BadRequestException("Oops! Looks like that user doesn't exist.");
    }

    const payload = { user_id };
    const token = this.jwtService.sign(payload, {
      secret: this.configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      expiresIn: '1h',
    });

    await this.sendResetPasswordEmail(email, token);

    return {
      user_id,
      execute_at: getCurrentDate(),
    };
  };

  sendResetPasswordEmail = async (email: string, token: string) => {
    const url = `http://example.com/reset-password?token=${token}`;
    await this.mailerService.sendMail({
      to: email,
      subject: 'Reset Password',
      template: 'reset-password',
      context: {
        url,
      },
    });
  };

  handleResetPassword = async (token: string, newPassword: string) => {
    try {
      const decoded = this.jwtService.verify(token, {
        secret: this.configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      });
      const userId = decoded.user_id;
      return await this.usersService.changePassword(userId, newPassword);
    } catch (e) {
      throw new Error('Invalid or expired password reset token.');
    }
  };
}
