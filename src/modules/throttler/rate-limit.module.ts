import { Module } from '@nestjs/common';
import { ThrottlerModule } from '@nestjs/throttler';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ThrottlerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => [
        {
          ttl: +config.get<string>('THROTTLE_TTL'),
          limit: +config.get<string>('THROTTLE_LIMIT'),
        },
      ],
    }),
  ],
  exports: [ThrottlerModule],
})
export class RateLimitModule {}
