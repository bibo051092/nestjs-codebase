import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => ({
        transport: {
          host: config.get<string>('MAIL_HOST'),
          port: +config.get<string>('MAIL_PORT'),
          ignoreTLS: config.get<boolean>('MAIL_IGNORE_TLS'),
          secure: config.get<boolean>('MAIL_SECURE'),
          // auth: {
          //   user: config.get<string>('MAIL_USER'),
          //   pass: config.get<string>('MAIL_PASS'),
          // },
        },
        defaults: {
          from: config.get<string>('MAIL_FROM'),
        },
        template: {
          dir: process.cwd() + '/src/modules/email/templates/',
          adapter: new PugAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
  ],
})
export class EmailModule {}
