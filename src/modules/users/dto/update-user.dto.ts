import { ApiProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateUserDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  first_name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  last_name: string;
}

export class UserDataDto {
  @ApiProperty({ type: String, example: '2' })
  user_id: string;

  @ApiProperty({ type: String, example: '2024-05-12T05:12:58.143Z' })
  updated_at: string;
}

export class UpdateUserResponseDto {
  @ApiProperty({ type: Number, example: HttpStatus.OK })
  statusCode: number;

  @ApiProperty({ type: String, example: 'Update user by id' })
  message: string;

  @ApiProperty({ type: UserDataDto })
  data: UserDataDto;
}
