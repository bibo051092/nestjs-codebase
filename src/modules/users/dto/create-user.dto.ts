import { IsEmail, IsNotEmpty, IsString, IsStrongPassword } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { HttpStatus } from '@nestjs/common';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  first_name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({ type: String })
  last_name: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ type: String })
  email: string;

  @IsNotEmpty()
  @IsStrongPassword()
  @ApiProperty({ type: String })
  password: string;
}

export class UserDataDto {
  @ApiProperty({ type: String, example: '2' })
  user_id: string;

  @ApiProperty({ type: String, example: '2024-05-12T05:12:58.143Z' })
  created_at: string;
}

export class CreateUserResponseDto {
  @ApiProperty({ type: Number, example: HttpStatus.CREATED })
  statusCode: number;

  @ApiProperty({ type: String, example: 'Create new a user' })
  message: string;

  @ApiProperty({ type: UserDataDto })
  data: UserDataDto;
}
