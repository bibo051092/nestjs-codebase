export interface IUser {
  user_id: number;
  first_name: string;
  last_name: string;
  email: string;
  password?: string;
  refresh_token?: string;
  is_active?: boolean;
  created_at?: Date;
  updated_at?: Date;
}
