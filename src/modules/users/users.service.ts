import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '@modules/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from '@modules/users/dto/create-user.dto';
import { getCurrentDate, getHashPassword } from '@shared/utils/common.util';
import { UpdateUserDto } from '@modules/users/dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  create = async (createUserDto: CreateUserDto) => {
    // check duplicate email
    const isExistEmail = await this.findByEmail(createUserDto.email);
    if (isExistEmail) {
      throw new BadRequestException('Email already exists.');
    }

    // create new user
    const userCreated = await this.userRepository.save({
      ...createUserDto,
      password: getHashPassword(createUserDto.password),
    });

    return {
      user_id: userCreated.user_id,
      created_at: userCreated.created_at,
    };
  };

  findByEmail = async (email: string) => {
    return await this.userRepository.findOneBy({ email });
  };

  findById = async (user_id: number) => {
    const user = await this.userRepository.findOne({
      select: [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'is_active',
        'created_at',
        'updated_at',
      ],
      where: { user_id },
    });
    if (!user) {
      throw new BadRequestException("Oops! Looks like that user doesn't exist.");
    }

    return user;
  };

  updateById = async (user_id: number, updateUserDto: UpdateUserDto) => {
    const user = await this.findById(user_id);
    await this.userRepository.update({ user_id }, { ...updateUserDto });

    return {
      user_id,
      updated_at: user.updated_at,
    };
  };

  deleteById = async (user_id: number) => {
    await this.findById(user_id);
    await this.userRepository.delete({ user_id });

    return {
      user_id,
      deleted_at: getCurrentDate(),
    };
  };

  getUsers = async (page: number, limit: number, sort: string, filters: Record<string, any>) => {
    const [field, order] = sort.split(',');
    const [results, total] = await this.userRepository.findAndCount({
      select: [
        'user_id',
        'first_name',
        'last_name',
        'email',
        'is_active',
        'created_at',
        'updated_at',
      ],
      where: filters,
      take: limit,
      skip: (page - 1) * limit,
      order: {
        [field]: order.toUpperCase() === 'DESC' ? 'DESC' : 'ASC',
      },
    });

    return {
      meta: {
        current: page,
        pageSize: limit,
        total,
        pages: Math.ceil(total / limit),
      },
      data: results,
    };
  };

  updateRefreshToken = async (refresh_token: string, user_id: number) => {
    const user = await this.findById(user_id);
    await this.userRepository.update(user_id, { refresh_token });

    return {
      user_id,
      updated_at: user.updated_at,
    };
  };

  findUserByRefreshToken = async (refresh_token: string) => {
    const user = await this.userRepository.findOneBy({ refresh_token });
    if (!user) {
      throw new BadRequestException("Oops! Looks like that user doesn't exist.");
    }

    return user;
  };

  changePassword = async (user_id: number, new_password: string) => {
    const user = await this.findById(user_id);
    await this.userRepository.update(user_id, { password: getHashPassword(new_password) });

    return {
      user_id,
      updated_at: user.updated_at,
    };
  };
}
