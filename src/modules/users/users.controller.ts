import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { ResponseMessage } from '@shared/decorators/common.decorator';
import { CreateUserDto, CreateUserResponseDto } from '@modules/users/dto/create-user.dto';
import { UpdateUserDto, UpdateUserResponseDto } from '@modules/users/dto/update-user.dto';
import {
  CURRENT_PAGE_PAGINATION_DEFAULT,
  LIMIT_PAGINATION_DEFAULT,
  SORT_PAGINATION_DEFAULT,
} from '@shared/constants/common.constant';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ErrorResponseDto } from '@shared/dto/common.dto';
import { exampleErrorResponse, exampleSuccessResponse } from '@shared/utils/common.util';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ResponseMessage('Create new a user')
  @Post()
  @ApiOperation({ summary: 'Create new a user' })
  @ApiResponse(exampleSuccessResponse(HttpStatus.CREATED, CreateUserResponseDto))
  @ApiResponse(exampleErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ErrorResponseDto))
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @ResponseMessage('Get user by id')
  @Get(':id')
  findOne(@Param('id') user_id: string) {
    return this.usersService.findById(+user_id);
  }

  @ResponseMessage('Update user by id')
  @Patch(':id')
  @ApiOperation({ summary: 'Update user by id' })
  @ApiResponse(exampleSuccessResponse(HttpStatus.OK, UpdateUserResponseDto))
  @ApiResponse(exampleErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ErrorResponseDto))
  update(@Param('id') user_id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.updateById(+user_id, updateUserDto);
  }

  @ResponseMessage('Delete user by id')
  @Delete(':id')
  remove(@Param('id') user_id: string) {
    return this.usersService.deleteById(+user_id);
  }

  @ResponseMessage('Get list users')
  @Get()
  findAll(
    @Query('page') page: string = CURRENT_PAGE_PAGINATION_DEFAULT.toString(),
    @Query('limit') limit: string = LIMIT_PAGINATION_DEFAULT.toString(),
    @Query('sort') sort: string = SORT_PAGINATION_DEFAULT.toString(),
    @Query('filters') filters: string = '{}',
  ) {
    return this.usersService.getUsers(+page, +limit, sort, JSON.parse(filters));
  }
}
