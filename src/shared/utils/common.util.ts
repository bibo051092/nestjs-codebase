import { compareSync, genSaltSync, hashSync } from 'bcryptjs';

export const getHashPassword = (password: string): string => {
  const salt = genSaltSync(10);
  return hashSync(password, salt);
};

export const checkPassword = (password: string, hash: string): boolean => {
  return compareSync(password, hash);
};

export const getCurrentDate = () => {
  return new Date().toISOString();
};

export const exampleSuccessResponse = (status: number, type: any) => {
  return {
    status: status,
    description: 'Successfully Response.',
    type: type,
  };
};

export const exampleErrorResponse = (status: number, type: any) => {
  return {
    status: status,
    description: 'Error Response',
    type: type,
  };
};
