import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from '@modules/users/users.module';
import { envConfig } from '@config/env.config';
import { DatabaseModule } from '@modules/database/database.module';
import { CacheRedisModule } from '@modules/cache/redis.module';
import { ThrottlerModule } from '@nestjs/throttler';
import { LoggerModule } from '@modules/logger/logger.module';
import { MorganMiddleware } from '@modules/logger/morgan.middleware';
import { AuthModule } from '@modules/auth/auth.module';
import { EmailModule } from '@modules/email/email.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: process.env.NODE_ENV == 'production' ? undefined : '.env',
      load: [envConfig],
    }),
    LoggerModule,
    ThrottlerModule,
    DatabaseModule,
    CacheRedisModule,
    UsersModule,
    AuthModule,
    EmailModule,
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(MorganMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
