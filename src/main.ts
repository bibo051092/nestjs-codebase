import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { ValidationConfig } from '@config/validation.config';
import { TransformInterceptor } from '@shared/interceptors/transform.interceptor';
import helmet from 'helmet';
import { cspConfig } from '@config/csp.config';
import { corsConfig } from '@config/cors.config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { JwtAuthGuard } from '@modules/auth/jwt/jwt-auth.guard';
import cookieParser from 'cookie-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  const configService = app.get(ConfigService);
  const reflector = app.get(Reflector);

  // Config Validation
  app.useGlobalPipes(new ValidationPipe(ValidationConfig));

  // Config JWT
  app.useGlobalGuards(new JwtAuthGuard(reflector));

  // Transform Response
  app.useGlobalInterceptors(new TransformInterceptor(reflector));

  // Config Cookies
  app.use(cookieParser());

  // Config CSP
  app.use(helmet(cspConfig));

  // Config CORS
  app.enableCors(corsConfig);

  // Config Versioning
  app.setGlobalPrefix('api');
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: ['1'],
  });

  // Config Swagger
  if (process.env.NODE_ENV === 'development') {
    const config = new DocumentBuilder()
      .setTitle('Nestjs Base API')
      .setDescription('The API description')
      .setVersion('1.0')
      .addTag('example')
      .addBearerAuth()
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('swagger/api', app, document);
  }

  await app.listen(+configService.get<string>('PORT'));
}
bootstrap();
