## Feature list
- [x] Database (PostgreSQL, TypeORM)
- [x] Caching (Redis store)
- [x] Sign in and sign up (JWT)
- [x] Forgot password
- [ ] Support multi-language i18n
- [x] Validation (request params, query..)
- [x] Docker installation
- [ ] CI/CD
- [x] Swagger for API document
- [x] Linter with Eslint for Typescript
- [x] Logger with `Morgan`
- [x] Debugger with `Winston`